 import Nav from './components/nav'
 import Home from './components/nav/Home'
import Payment from './components/nav/Payment'
import Movies from './components/nav/Movies'
import TvShows from './components/nav/TvShows'
import Details from './components/nav/viewDetail'
 
import './App.css';
import { BrowserRouter,Routes, Route } from 'react-router-dom';
 
function App() {
  return (
   <>

    <BrowserRouter>
    <Nav />
    
    <Routes>
<Route path='/' exact element={<Home />} /> 
<Route path='/payment' element={<Payment />} /> 
<Route path='/movies' element={<Movies />} /> 
<Route path='/tvshows' element={<TvShows />} />
<Route path='/details/:id' element={<Details />} />
</Routes>
    </BrowserRouter>
   </>
  );
}

export default App;
