import React from "react";
import { useParams } from "react-router-dom";

const Index = () => {
  const { id } = useParams();
  return (
    <>
      <div className=" inner-screen bg-slate-200 h-screen w-screen">
      <div  className="flex">
        <div className="left pl-9 pt-5">
          <img src="/images/holly2M.jpg" alt="" />
        </div>
        <div className="right pl-9 pt-5">
          {id}
          <div className="n flex space-x-2">
            <p>Movie Name</p>
            <button className="bg-orange-700 px-9 py-0 rounded-md">
              Button0
            </button>
          </div>
          <div className="buttons space-x-1 mt-2">
            <button className="bg-orange-700 px-1 py-2 rounded-md">
              Button1
            </button>
            <button className="bg-orange-700 px-5 py-3 rounded-md">
              Button2
            </button>
          </div>
          <p className="mt-7">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis
            maxime aut enim voluptas non vel temporibus. Velit tenetur ea
            numquam asperiores voluptatem enim? Libero porro natus quis sit,
            suscipit repellendus cupiditate modi, magnam totam facere maiores
            fuga aut ut minima, eligendi suscipit neque! Soluta iure laborum
            eius quae fuga?
          </p>
          <button className="bg-orange-700 px-4 py-1 rounded-md">watch</button>
        </div>
</div>
        <div className="trailers  mt-14">
          <h1 className="ml-60 font-bold text-xl text-rose-700">TRAILERS</h1>
          <div className="m flex space-x-6 ml-36 mt-8">
            <div>
              <img src="/images/playIcon.png" alt="" className="h-56 border-solid border-2 border-indigo-600 rounded-lg" />
            </div>

            <div>
              <img src="/images/palyIcon2.jpg" alt="" className="h-56  border-solid border-2 border-indigo-600 rounded-lg " />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
