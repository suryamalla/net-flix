import React from 'react'
import Card from '../../card'
import {NavLink} from 'react-router-dom'
 

 let id=7;
 
const index = () => {

  return (
    <> 
       <div className='inner-screen tvshows bg-slate-700 text-white space-x-9 py-8 h-screen'>
                <h1 className='text-3xl space-x-5 ml-7'>Tvshows</h1>
               
                <div className="listfortvshow flex space-x-10 mt-10 pb-9" 
                  
                  >
               <NavLink to={`/details/${id}`} >
                <Card 
                id="1"
                    name="POSTER CREATER"
                    imagefile='images/Tvshows1.jpg'
                   
                />
                </NavLink>

                <NavLink to={`/details/${id}`} >
                <Card 
                id="2"
                     name="THE TWILIGHT"
                    imagefile="images/Tvshows2.jpg"
                />
                </NavLink>

                <NavLink to={`/details/${id}`} >
                <Card 
                id="3"
                     name="REGINA"
                    imagefile="images/Tvshows3.jpg"
                />
                 </NavLink>
                </div>
                
                
            </div>
           
    </>
  )
}

export default index