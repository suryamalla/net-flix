import React from "react";
import Card from "../../card";
import { NavLink } from "react-router-dom";

let id = 7;

const index = () => {
  return (
    <>
      <div className="inner-screen tvshows bg-slate-800 text-white space-x-9 py-8 h-screen">
        <h1 className="text-3xl space-x-5 ml-7">Movies</h1>
        <div className="listfortvshow flex space-x-10 mt-10 pb-9">
          <NavLink to={`/details/${id}`}>
            <Card name="ROADTOATY" imagefile="images/holly2M.jpg" />
          </NavLink>

          <NavLink to={`/details/${id}`}>
            <Card name="THE RUNNER" imagefile="images/holly1M.jpg" />
          </NavLink>

          <NavLink to={`/details/${id}`}>
            <Card name="CRYPT" imagefile="images/holly3M.jpg" />
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default index;
