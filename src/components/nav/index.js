import React from 'react'
import {NavLink} from 'react-router-dom'
const index = () => {
  return (
   <>
   <div className="navbar bg-slate-900 flex text-white  py-5 items-center space-x-16">
    <div className="logo ml-6" ><NavLink to='/'><img src="images/logo.png" alt="" className='h-9 rounded-lg' /></NavLink> </div>
    <div className="sections">
        <ul className='flex space-x-10'>
            <li>
                <NavLink to='/payment'  className={({ isActive }) => (isActive ? 'underline' : 'no-underline')}>Payment</NavLink>
            </li>
            <li>
                <NavLink to='/' className={({ isActive }) => (isActive ? 'underline' : 'no-underline')}>Home</NavLink>
            </li>
            <li>
                <NavLink to='/tvshows' className={({ isActive }) => (isActive ? 'underline' : 'no-underline')} >TvShows</NavLink>
            </li>
            <li>
                <NavLink to='/movies' className={({ isActive }) => (isActive ? 'underline' : 'no-underline')} >Movies</NavLink>
            </li>
        </ul>
    </div>
</div>


   </>
  )
}

export default index