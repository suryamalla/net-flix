import React from "react";
import Banner from "../../banner";
import Card from "../../card";
import { NavLink } from "react-router-dom";
let id = "5";
const index = () => {
  return (
    <>
      <div className="mouter">
        <Banner />

        <div className="outer ">
          <div className="tvshows bg-slate-800 text-white space-x-9 py-8">
            <h1 className="text-3xl space-x-5 ml-7">Movies</h1>
            <div className="listfortvshow flex space-x-10 mt-10 pb-9">
              <NavLink to={`/details/${id}`}>
                <Card name="ROADTOATY" imagefile="images/holly2M.jpg" />
              </NavLink>

              <NavLink to={`/details/${id}`}>
                <Card name="THE RUNNER" imagefile="images/holly1M.jpg" />
              </NavLink>

              <NavLink to={`/details/${id}`}>
                <Card name="CRYPT" imagefile="images/holly3M.jpg" />
              </NavLink>
            </div>
          </div>

          <div className="tvshows  text-white space-x-9 py-8 bg-slate-700">
            <h1 className="text-3xl ml-7">Tvshows</h1>
            <div className="listfortvshow flex space-x-10 mt-10 pb-9">
              <NavLink to={`/details/${id}`}>
                <Card name="POSTER CREATER" imagefile="images/Tvshows1.jpg" />
              </NavLink>

              <NavLink to={`/details/${id}`}>
                <Card name="THE TWILIGHT" imagefile="images/Tvshows2.jpg" />
              </NavLink>

              <NavLink to={`/details/${id}`}>
                <Card name="REGINA" imagefile="images/Tvshows3.jpg" />
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default index;
