import React from 'react'

const index = () => {
  return (
    <div className='banner min-h-[500px] relative flex items-center text-white'>
    <img src='Banner1.jpg' alt="" className='absolute inset-0 bg-cover max-h-full w-full'  />
    <div className="container text-center relative z-9">
        <h1 className='!text-[48px] mb-5'>Movie Library</h1>
        <p className='text-lg'>Download and Watch Movies and Tvshows</p>
    </div>
    </div>
  )
}

export default index