import React from "react";

 
const index = (props) => {
  
   
  return (
    <>
      <span className="cursor-pointer" >
        {props.name}
        <img src={props.imagefile} alt="" className="h-48" />
      </span>
    </>
  );
};

export default index;
